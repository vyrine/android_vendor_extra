ifeq ($(TARGET_NUKE), true)
PRODUCT_PACKAGES += nuke
endif

ifeq ($(TARGET_INCLUDE_MULCH_WEBVIEW), true)
PRODUCT_PACKAGES += \
    MulchSystemWebView \
    MulchSystemWebViewOverlay
endif

ifeq ($(TARGET_INCLUDE_GBOARD), true)
PRODUCT_PACKAGES += Gboard
endif
