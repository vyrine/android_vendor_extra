#!/usr/bin/env bash
#
# Copyright (C) 2022 Giovanni Ricca
#
# SPDX-License-Identifier: Apache-2.0
#

LOS_VERSION=$(grep "PRODUCT_VERSION_MAJOR" "$(gettop)/vendor/lineage/config/version.mk" | sed 's/PRODUCT_VERSION_MAJOR = //g' | head -1)
VENDOR_EXTRA_PATH=$(gettop)/vendor/extra
VENDOR_PATCHES_PATH="${VENDOR_EXTRA_PATH}"/patches
VENDOR_PATCHES_PATH_VERSION="${VENDOR_PATCHES_PATH}"/lineage-"${LOS_VERSION}"
VENDOR_EMOJI_PATH="${VENDOR_EXTRA_PATH}"/emoji

# Apply patches
if [[ "${APPLY_VENDOR_EXTRA_PATCHES}" == "true" ]]; then
    for project_name in $(
        cd "${VENDOR_PATCHES_PATH_VERSION}" || exit
        echo */
    ); do
        project_path="$(tr _ / <<< "$project_name")"

        cd "$(gettop)/${project_path}" || exit
        git am "${VENDOR_PATCHES_PATH_VERSION}"/"${project_name}"/*.patch
        git am --abort &> /dev/null
    done

    # Return to source rootdir
    croot
fi

# Replace Emoji
if [[ "${REPLACE_EMOJI}" == "true" ]]; then
    (
        font_dir="$(gettop)/external/noto-fonts/emoji-compat/font"
        if [ -d "$font_dir" ]; then
            cd "$font_dir" || exit
            cp "${VENDOR_EMOJI_PATH}"/Twemoji.ttf NotoColorEmojiCompat.ttf
            git clean -q -f
        else
            echo "Replace Emoji: $font_dir not found."
        fi
    )
fi
